/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part1;

/**
 *
 * @author dannylowpass
 */
public class PayrollSimulation {
    
    public static void main(String[] args){
        
        Employee emp = new Employee(Johnny, 15.25, 40);
        Manager mger = new Employee(Charlie, 25, 45, 500);
        
        System.out.println(emp.calculatePay());
        System.out.println(mger.calculatePay());
    }
    
}
