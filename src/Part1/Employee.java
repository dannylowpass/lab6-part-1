/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part1;

/**
 *
 * @author dannylowpass
 */
public class Employee {
    
    String name;
    double hourlyWage;
    double hoursWorked;
    
    public Employee(){
        
    }

    public Employee(double hourlyWage, double hoursWorked){
        this.hourlyWage = hourlyWage;
        this.hoursWorked = hoursWorked;
    }
    
    public Employee(String name, double hourlyWage, double hoursWorked){
        
        this.name = name;
        this.hourlyWage = hourlyWage;
        this.hoursWorked = hoursWorked;
        
    }
    
    public String getName(){
        return name;
    }
    
    public double getHourlyWage(){
        return hourlyWage;
       
    }
    
    public double getHoursWorked(){
        return hoursWorked;
    }
    
    public void setName(String newName){
        this.name = newName;
    }
    
    public void setHourlyWage(double newWage){
        this.hourlyWage = newWage;
    }
    
    public void setHoursWorked(double newHoursWorked){
        this.hoursWorked = newHoursWorked;
    }
    
    public double calculatePay(){
        return hoursWorked * hourlyWage;
    }
}
